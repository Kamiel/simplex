//
//  main.m
//  SimpleTODO
//
//  Created by Yuriy Pitomets on 11/14/14.
//  Copyright (c) 2014 Simplex Solutions Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
