//
//  AppDelegate.h
//  SimpleTODO
//
//  Created by Yuriy Pitomets on 11/14/14.
//  Copyright (c) 2014 Simplex Solutions Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

